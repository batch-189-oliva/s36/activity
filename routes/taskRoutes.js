const express = require('express')
const TaskController = require('../controllers/TaskController.js')
const router = express.Router()


router.get('/:id', (request, response) => {
	TaskController.getTask(request.params.id).then((findTask) => response.send(findTask))
})


router.put('/:id/complete', (request, response) => {
	TaskController.updateTask(request.params.id, request.body).then((updatedTask) => response.send(updatedTask))
})

module.exports = router
const Task = require('../models/Task.js')

module.exports.getTask = (taskId) => {
	return Task.findById(taskId).then((findTask, error) => {
		if(error){
			return error
		}

		return findTask
	})
}


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		if(error){
			return error
		}

		result.status = newContent.status

		return result.save().then((updatedTask, error) => {
			if(error){
				return error
			}

			return updatedTask
		})
	})
}
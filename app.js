const express = require('express')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/taskRoutes.js')

dotenv.config()

const app = express()
const port = 3001
app.use(express.json())
app.use(express.urlencoded({extended: true}))

mongoose.connect(`mongodb+srv://admin123:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.sr40tbb.mongodb.net/S36-Discussion?retryWrites=true&w=majority`,
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
)

let db = mongoose.connection
db.on('error', () => console.error('Connection Error :('))
db.on('open', () => console.log('Connected to MongoDB!'))

app.use('/api/tasks', taskRoutes)

app.listen(port, () => console.log(`Server is running at port ${port}`))